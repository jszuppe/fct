# cce1dfa57bc9c5c0613520386642cd82
CFLAGS=-O$(O) -DBOOST_LOG_DYN_LINK -std=c++11 -Wall -Wextra
O=2
LFLAGS= -lboost_program_options -lboost_log -lboost_log_setup -pthread
OBJS=build/run.o build/futils.o build/AsmFileAnalyze.o build/main.o build/SysCall.o build/Iaddress.o build/FuncCall.o build/CallAddress.o build/OpcodeAndOperands.o build/Address.o build/Instruction.o build/SourceFile.o build/Instructions.o build/utils.o build/InstructionParser.o build/AddressParser.o build/CallAddressParser.o build/OpcodeAndOperandsParser.o build/InstructionsParser.o build/SourceFileParser.o

.PHONY: all
all: objs bin/fct

bin/fct: $(OBJS)
	@ mkdir -p bin
	@ echo "    LINK bin/fct"
	@ $(CXX) $(CFLAGS) $(OBJS) -o "bin/fct" $(LFLAGS)

build/run.o: src/run.cpp src/run.hpp src/AsmFileAnalyze.hpp \
 src/info/Iaddress.hpp src/futils.hpp src/common/utils.hpp \
 src/productions/SourceFile.hpp src/productions/Production.hpp \
 src/productions/Instructions.hpp src/productions/../info/FuncCall.hpp \
 src/productions/../info/SysCall.hpp src/productions/Instruction.hpp \
 src/productions/OpcodeAndOperands.hpp src/productions/CallAddress.hpp \
 src/productions/Address.hpp src/pfactories/SourceFileParser.hpp \
 src/pfactories/ProductionParser.hpp \
 src/pfactories/InstructionsParser.hpp \
 src/pfactories/InstructionParser.hpp src/pfactories/AddressParser.hpp \
 src/pfactories/OpcodeAndOperandsParser.hpp \
 src/pfactories/CallAddressParser.hpp
	@ echo "    CXX  src/run.cpp"
	@ $(CXX) $(CFLAGS)  -c "src/run.cpp" -o $@
build/futils.o: src/futils.cpp src/futils.hpp src/common/utils.hpp
	@ echo "    CXX  src/futils.cpp"
	@ $(CXX) $(CFLAGS) -c "src/futils.cpp" -o $@
build/AsmFileAnalyze.o: src/AsmFileAnalyze.cpp src/AsmFileAnalyze.hpp \
 src/info/Iaddress.hpp src/futils.hpp src/common/utils.hpp \
 src/productions/SourceFile.hpp src/productions/Production.hpp \
 src/productions/Instructions.hpp src/productions/../info/FuncCall.hpp \
 src/productions/../info/SysCall.hpp src/productions/Instruction.hpp \
 src/productions/OpcodeAndOperands.hpp src/productions/CallAddress.hpp \
 src/productions/Address.hpp src/pfactories/SourceFileParser.hpp \
 src/pfactories/ProductionParser.hpp \
 src/pfactories/InstructionsParser.hpp \
 src/pfactories/InstructionParser.hpp src/pfactories/AddressParser.hpp \
 src/pfactories/OpcodeAndOperandsParser.hpp \
 src/pfactories/CallAddressParser.hpp
	@ echo "    CXX  src/AsmFileAnalyze.cpp"
	@ $(CXX) $(CFLAGS) -c "src/AsmFileAnalyze.cpp" -o $@
build/main.o: src/main.cpp src/run.hpp src/AsmFileAnalyze.hpp \
 src/info/Iaddress.hpp src/futils.hpp src/common/utils.hpp \
 src/productions/SourceFile.hpp src/productions/Production.hpp \
 src/productions/Instructions.hpp src/productions/../info/FuncCall.hpp \
 src/productions/../info/SysCall.hpp src/productions/Instruction.hpp \
 src/productions/OpcodeAndOperands.hpp src/productions/CallAddress.hpp \
 src/productions/Address.hpp src/pfactories/SourceFileParser.hpp \
 src/pfactories/ProductionParser.hpp \
 src/pfactories/InstructionsParser.hpp \
 src/pfactories/InstructionParser.hpp src/pfactories/AddressParser.hpp \
 src/pfactories/OpcodeAndOperandsParser.hpp \
 src/pfactories/CallAddressParser.hpp
	@ echo "    CXX  src/main.cpp"
	@ $(CXX) $(CFLAGS) -c "src/main.cpp" -o $@
build/SysCall.o: src/info/SysCall.cpp src/info/SysCall.hpp \
 src/info/Iaddress.hpp
	@ echo "    CXX  src/info/SysCall.cpp"
	@ $(CXX) $(CFLAGS) -c "src/info/SysCall.cpp" -o $@
build/Iaddress.o: src/info/Iaddress.cpp src/info/Iaddress.hpp
	@ echo "    CXX  src/info/Iaddress.cpp"
	@ $(CXX) $(CFLAGS) -c "src/info/Iaddress.cpp" -o $@
build/FuncCall.o: src/info/FuncCall.cpp src/info/FuncCall.hpp \
 src/info/Iaddress.hpp
	@ echo "    CXX  src/info/FuncCall.cpp"
	@ $(CXX) $(CFLAGS) -c "src/info/FuncCall.cpp" -o $@
build/CallAddress.o: src/productions/CallAddress.cpp \
 src/productions/CallAddress.hpp src/productions/Production.hpp
	@ echo "    CXX  src/productions/CallAddress.cpp"
	@ $(CXX) $(CFLAGS) -c "src/productions/CallAddress.cpp" -o $@
build/OpcodeAndOperands.o: src/productions/OpcodeAndOperands.cpp \
 src/productions/OpcodeAndOperands.hpp src/productions/Production.hpp \
 src/productions/CallAddress.hpp
	@ echo "    CXX  src/productions/OpcodeAndOperands.cpp"
	@ $(CXX) $(CFLAGS) -c "src/productions/OpcodeAndOperands.cpp" -o $@
build/Address.o: src/productions/Address.cpp src/productions/Address.hpp \
 src/productions/Production.hpp
	@ echo "    CXX  src/productions/Address.cpp"
	@ $(CXX) $(CFLAGS) -c "src/productions/Address.cpp" -o $@
build/Instruction.o: src/productions/Instruction.cpp \
 src/productions/Instruction.hpp src/productions/Production.hpp \
 src/productions/OpcodeAndOperands.hpp src/productions/CallAddress.hpp \
 src/productions/Address.hpp
	@ echo "    CXX  src/productions/Instruction.cpp"
	@ $(CXX) $(CFLAGS) -c "src/productions/Instruction.cpp" -o $@
build/SourceFile.o: src/productions/SourceFile.cpp \
 src/productions/SourceFile.hpp src/productions/Production.hpp \
 src/productions/Instructions.hpp src/productions/../info/FuncCall.hpp \
 src/productions/../info/Iaddress.hpp src/productions/../info/SysCall.hpp \
 src/productions/Instruction.hpp src/productions/OpcodeAndOperands.hpp \
 src/productions/CallAddress.hpp src/productions/Address.hpp
	@ echo "    CXX  src/productions/SourceFile.cpp"
	@ $(CXX) $(CFLAGS) -c "src/productions/SourceFile.cpp" -o $@
build/Instructions.o: src/productions/Instructions.cpp \
 src/productions/Instructions.hpp src/productions/Production.hpp \
 src/productions/../info/FuncCall.hpp \
 src/productions/../info/Iaddress.hpp src/productions/../info/SysCall.hpp \
 src/productions/Instruction.hpp src/productions/OpcodeAndOperands.hpp \
 src/productions/CallAddress.hpp src/productions/Address.hpp
	@ echo "    CXX  src/productions/Instructions.cpp"
	@ $(CXX) $(CFLAGS) -c "src/productions/Instructions.cpp" -o $@
build/utils.o: src/common/utils.cpp src/common/utils.hpp
	@ echo "    CXX  src/common/utils.cpp"
	@ $(CXX) $(CFLAGS) -c "src/common/utils.cpp" -o $@
build/InstructionParser.o: src/pfactories/InstructionParser.cpp \
 src/pfactories/InstructionParser.hpp src/pfactories/ProductionParser.hpp \
 src/pfactories/../productions/Production.hpp \
 src/pfactories/../futils.hpp src/pfactories/../common/utils.hpp \
 src/pfactories/AddressParser.hpp \
 src/pfactories/../productions/Address.hpp \
 src/pfactories/OpcodeAndOperandsParser.hpp \
 src/pfactories/CallAddressParser.hpp \
 src/pfactories/../productions/CallAddress.hpp \
 src/pfactories/../productions/OpcodeAndOperands.hpp \
 src/pfactories/../productions/Instruction.hpp
	@ echo "    CXX  src/pfactories/InstructionParser.cpp"
	@ $(CXX) $(CFLAGS) -c "src/pfactories/InstructionParser.cpp" -o $@
build/AddressParser.o: src/pfactories/AddressParser.cpp \
 src/pfactories/AddressParser.hpp src/pfactories/ProductionParser.hpp \
 src/pfactories/../productions/Production.hpp \
 src/pfactories/../futils.hpp src/pfactories/../common/utils.hpp \
 src/pfactories/../productions/Address.hpp
	@ echo "    CXX  src/pfactories/AddressParser.cpp"
	@ $(CXX) $(CFLAGS) -c "src/pfactories/AddressParser.cpp" -o $@
build/CallAddressParser.o: src/pfactories/CallAddressParser.cpp \
 src/pfactories/CallAddressParser.hpp src/pfactories/ProductionParser.hpp \
 src/pfactories/../productions/Production.hpp \
 src/pfactories/../futils.hpp src/pfactories/../common/utils.hpp \
 src/pfactories/AddressParser.hpp \
 src/pfactories/../productions/Address.hpp \
 src/pfactories/../productions/CallAddress.hpp
	@ echo "    CXX  src/pfactories/CallAddressParser.cpp"
	@ $(CXX) $(CFLAGS) -c "src/pfactories/CallAddressParser.cpp" -o $@
build/OpcodeAndOperandsParser.o: src/pfactories/OpcodeAndOperandsParser.cpp \
 src/pfactories/OpcodeAndOperandsParser.hpp \
 src/pfactories/ProductionParser.hpp \
 src/pfactories/../productions/Production.hpp \
 src/pfactories/../futils.hpp src/pfactories/../common/utils.hpp \
 src/pfactories/CallAddressParser.hpp src/pfactories/AddressParser.hpp \
 src/pfactories/../productions/Address.hpp \
 src/pfactories/../productions/CallAddress.hpp \
 src/pfactories/../productions/OpcodeAndOperands.hpp
	@ echo "    CXX  src/pfactories/OpcodeAndOperandsParser.cpp"
	@ $(CXX) $(CFLAGS) -c "src/pfactories/OpcodeAndOperandsParser.cpp" -o $@
build/InstructionsParser.o: src/pfactories/InstructionsParser.cpp \
 src/pfactories/InstructionsParser.hpp \
 src/pfactories/ProductionParser.hpp \
 src/pfactories/../productions/Production.hpp \
 src/pfactories/../futils.hpp src/pfactories/../common/utils.hpp \
 src/pfactories/InstructionParser.hpp src/pfactories/AddressParser.hpp \
 src/pfactories/../productions/Address.hpp \
 src/pfactories/OpcodeAndOperandsParser.hpp \
 src/pfactories/CallAddressParser.hpp \
 src/pfactories/../productions/CallAddress.hpp \
 src/pfactories/../productions/OpcodeAndOperands.hpp \
 src/pfactories/../productions/Instruction.hpp \
 src/pfactories/../productions/Instructions.hpp \
 src/pfactories/../productions/../info/FuncCall.hpp \
 src/pfactories/../productions/../info/Iaddress.hpp \
 src/pfactories/../productions/../info/SysCall.hpp
	@ echo "    CXX  src/pfactories/InstructionsParser.cpp"
	@ $(CXX) $(CFLAGS) -c "src/pfactories/InstructionsParser.cpp" -o $@
build/SourceFileParser.o: src/pfactories/SourceFileParser.cpp \
 src/pfactories/SourceFileParser.hpp src/pfactories/ProductionParser.hpp \
 src/pfactories/../productions/Production.hpp \
 src/pfactories/../futils.hpp src/pfactories/../common/utils.hpp \
 src/pfactories/InstructionsParser.hpp \
 src/pfactories/InstructionParser.hpp src/pfactories/AddressParser.hpp \
 src/pfactories/../productions/Address.hpp \
 src/pfactories/OpcodeAndOperandsParser.hpp \
 src/pfactories/CallAddressParser.hpp \
 src/pfactories/../productions/CallAddress.hpp \
 src/pfactories/../productions/OpcodeAndOperands.hpp \
 src/pfactories/../productions/Instruction.hpp \
 src/pfactories/../productions/Instructions.hpp \
 src/pfactories/../productions/../info/FuncCall.hpp \
 src/pfactories/../productions/../info/Iaddress.hpp \
 src/pfactories/../productions/../info/SysCall.hpp \
 src/pfactories/../productions/SourceFile.hpp
	@ echo "    CXX  src/pfactories/SourceFileParser.cpp"
	@ $(CXX) $(CFLAGS) -c "src/pfactories/SourceFileParser.cpp" -o $@

objs:
	@ mkdir -p "build"
.PHONY: c clean
c: clean
clean:
	@ if [ -d "build" ]; then rm -r "build"; fi
	@ if [ -d "bin" ]; then rm -r "bin"; fi
	@ echo "    CLEAN"
.PHONY: f fresh
f: fresh
fresh: clean
	@ make all --no-print-directory
