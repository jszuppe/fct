# Treść zadania #

Celem zadania jest sparsowanie pliku wejściowego z kodem w języku assembler, wykrycie
wszystkich wywołań funkcji adresowanych bezpośrednio (i wybranych pośrednich) oraz
wykrycie/oszacowanie początku i końca funkcji. Wyniki powinny być zapisane w plikach,
zawierającym informacje o pozycji instrukcji skoku i jej celu (wersja bezpośrednio adresowana
i dotycząca wywołań systemowych) oraz pliku zawierającym oszacowany początek i koniec funkcji.

# Wymagania #

* Kompilator zgodny z C++11 (testowany był g++ 4.7)
* Biblioteka Boost 1.55
