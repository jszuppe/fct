/*
 * AsmFileAnalyze.cpp
 *
 *  Created on: 10 kwi 2014
 *      Author: jakub
 */

#include "AsmFileAnalyze.hpp"

AsmFileAnalyze::AsmFileAnalyze(std::string fileNamePath)
{
	this->inputFile.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	try
	{
		this->inputFile.open(fileNamePath.c_str());
		this->inputFile.exceptions(std::ifstream::badbit);
	} catch (...)
	{
		std::throw_with_nested(std::logic_error("Error occurred while opening file: " + fileNamePath));
	}
}

AsmFileAnalyze::~AsmFileAnalyze()
{

}

void AsmFileAnalyze::run()
{
	SourceFileParser sfp;
	source = std::unique_ptr<SourceFile>(static_cast<SourceFile*>(sfp.safeParse(inputFile)));
	buildResults();
}

void AsmFileAnalyze::buildResults()
{
	auto funcCallsList = source->getFuncCallsList();
	auto retList = source->getRetList();
	std::list<Iaddress> funcsStartAddr;
	for (auto i : funcCallsList)
	{
		funcsStartAddr.push_back(i.funcAddress);
	}
	retList.sort();
	funcsStartAddr.sort();

	auto retB = retList.begin();
	auto funB = funcsStartAddr.begin();

	auto retE = retList.end();
	auto funE = funcsStartAddr.end();

	auto funN = std::next(funB);
	auto retN = std::next(retB);

	/*
	 Adresy końcowe określane są według poniższego algorytmu:
	 * Niech zbiór adresów początkowych funkcji i zbiór adresów instrukcji ret będą
	 uporządkowane rosnąco,
	 * Wtedy adres końcowy funkcji N jest największym adresem instrukcji ret, który jest
	 jednocześnie mniejszy od adresu początkowego funkcji N+1, lub adresem ostatniej
	 instrukcji ret, jeżeli nie ma funkcji N+1.
	 */
	while (funB != funE)
	{
		if (retN == retE ||
				(funN != funE && *funN < *retN))
		{
			funcs.push_back(std::make_pair(*funB, *retB));
			funB++;
			funN = std::next(funB);
		}
		retB++;
		retN = std::next(retB);
	}
}

void AsmFileAnalyze::saveResults(AsmFileAnalyze::outputFiles fileNames)
{
	std::ofstream calls, funcs, sys;
	calls.exceptions(std::ofstream::failbit | std::ofstream::badbit);
	funcs.exceptions(std::ofstream::failbit | std::ofstream::badbit);
	sys.exceptions(std::ofstream::failbit | std::ofstream::badbit);
	try
	{
		calls.open(fileNames.callsFileName, std::ios_base::out | std::ios_base::trunc);
		funcs.open(fileNames.funcsFileName, std::ios_base::out | std::ios_base::trunc);
		sys.open(fileNames.syscallsFileName, std::ios_base::out | std::ios_base::trunc);

		for (auto i : this->funcs)
		{
			funcs << i.first.addrStr << "\t" << i.second.addrStr << "\n";
		}
		for (auto i : this->source->getFuncCallsList())
		{
			calls << i.address.addrStr << "\t" << i.funcAddress.addrStr << "\n";
		}
		for (auto i : this->source->getSysCallsList())
		{
			sys << i.address.addrStr << "\t" << i.wordSize << " "
					<< i.segment << " " << i.funcAddress.addrStr << "\n";
		}

	} catch (...)
	{
		std::throw_with_nested(std::logic_error("Error occurred while saving."));
	}

}
