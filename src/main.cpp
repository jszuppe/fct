/*
 *
 * main.cpp
 *
 * Zadanie 1
 * Celem zadania jest sparsowanie pliku wejściowego z kodem w języku assembler, wykrycie wszystkich
 * wywołań funkcji adresowanych bezpośrednio (i wybranych pośrednich) oraz wykrycie/oszacowanie
 * początku i końca funkcji. Wyniki powinny być zapisane w plikach, zawierającym informacje o pozycji
 * instrukcji skoku i jej celu (wersja bezpośrednio adresowana i dotycząca wywołań systemowych) oraz
 * pliku zawierającym oszacowany początek i koniec funkcji.
 *
 * Created on: 10 04 2014
 * Author: Jakub Szuppe
 *
 */

#include <iostream>
#include "run.hpp"

/**
 * Uruchomienie programu z odpowiednimi parametrami.
 *
 */
int main(int argc, char **argv)
{
	try
	{
		return run(argc, argv);
	}
	catch (std::exception& e)
	{
		std::cerr << "Unhandled Exception reached the top of main: "
				<< e.what() << ", application will now exit" << std::endl;
		return 1;
	}
	catch (...)
	{
		std::cerr << "Unhandled exception of unknown type reached the top of main. \nApplication will now exit"
				<< std::endl;
		return 1;
	}
}
