/*
 * CallAddressParser.hpp
 *
 *  Created on: 7 maj 2014
 *      Author: jakub
 */

#pragma once

#include "ProductionParser.hpp"
#include "AddressParser.hpp"
#include "../productions/CallAddress.hpp"
#include <algorithm>
#include <string>

class CallAddressParser: public ProductionParser
{
	private:

		AddressParser addressParser;

		static std::string wordSizes[4];
		static std::string segments[6];

	public:

		CallAddressParser();
		~CallAddressParser();
		Production * safeParse(std::ifstream& inputFile);

	private:

		inline bool isWordSize(std::string str)
		{
			std::string lower = str;
			std::transform(lower.begin(), lower.end(), lower.begin(), ::tolower);
			for (std::string word_size : wordSizes)
			{
				if (0 == lower.compare(word_size))
				{
					return true;
				}
			}
			return false;
		}

		inline bool isSegment(std::string str)
		{
			std::string lower = str.substr(0, 2);
			std::transform(lower.begin(), lower.end(), lower.begin(), ::tolower);
			for (std::string seg : segments)
			{
				if (0 == lower.compare(seg) && str[2] == ':')
				{
					return true;
				}
			}
			return false;
		}

		inline bool isPtr(std::string str)
		{
			std::string lower = str;
			std::transform(lower.begin(), lower.end(), lower.begin(), ::tolower);
			if (0 == lower.compare("ptr"))
			{
				return true;
			}
			return false;
		}

		inline bool isNumber(std::string str)
		{
			std::string lower = str;
			std::transform(lower.begin(), lower.end(), lower.begin(), ::tolower);
			if (0 == lower.compare("ptr"))
			{
				return true;
			}
			return false;
		}

		inline std::string parseAddress(std::ifstream& inputFile)
		{
			std::string addr;
			inpos svdpos = futils::currentPosition(inputFile);
			futils::safeGetLine(inputFile, addr);
			futils::restorePosition(inputFile, svdpos);

			std::string::size_type n = addr.find_first_not_of("0123456789abcdefABCDEF");
			addr = addr.substr(0,n);
			inputFile.ignore(n);
			svdpos = futils::currentPosition(inputFile);

			char c;
			futils::getChar(inputFile, &c);
			if(c != 'h' && c != 'H')
			{
				futils::restorePosition(inputFile, svdpos);
			}

			return addr;
		}
};

