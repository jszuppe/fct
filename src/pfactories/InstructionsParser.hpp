/*
 * InstructionsParser.hpp
 *
 *  Created on: 28 kwi 2014
 *      Author: jakub
 */

#pragma once

#include "ProductionParser.hpp"
#include "InstructionParser.hpp"

#include "../productions/Instruction.hpp"
#include "../productions/Instructions.hpp"

#include <list>

class InstructionsParser: public ProductionParser
{
	public:

		Production* safeParse(std::ifstream& inputFile);

	private:

		InstructionParser instructionParser;
};

