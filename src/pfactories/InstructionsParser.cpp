/*
 * InstructionsParser.cpp
 *
 *  Created on: 28 kwi 2014
 *      Author: jakub
 */

#include "InstructionsParser.hpp"

Production* InstructionsParser::safeParse(std::ifstream& inputFile)
{
	inpos svdpos = futils::currentPosition(inputFile);

	Instructions * instructions = new Instructions();
	std::shared_ptr<Instruction> i;
	try
	{
		while ((i = std::shared_ptr<Instruction>(static_cast<Instruction*>(instructionParser.safeParse(inputFile))))
				!= nullptr)
		{
			instructions->addInstruction(std::ref(*i));
			svdpos = futils::currentPosition(inputFile);
		}
		futils::restorePosition(inputFile, svdpos);
		return instructions;
	}
	catch (...)
	{
		delete instructions;
		futils::restorePosition(inputFile, svdpos);
		std::throw_with_nested(std::logic_error("Error occurred while InstructionsParser::safeParse"));
	}
	return nullptr;
}
