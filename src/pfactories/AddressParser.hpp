/*
 * AddressParser.hpp
 *
 *  Created on: 30 kwi 2014
 *      Author: jakub
 */

#pragma once

#include "ProductionParser.hpp"
#include "../productions/Address.hpp"

class AddressParser
{
	public:

		AddressParser();
		~AddressParser();
		Production* safeParse(std::ifstream& inputFile);
		static bool isAddress(const std::string& str);
};

