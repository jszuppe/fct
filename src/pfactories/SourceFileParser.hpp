/*
 * SourceFileFactory.hpp
 *
 *  Created on: 25 kwi 2014
 *      Author: jakub
 */

#pragma once

#include "ProductionParser.hpp"
#include "InstructionsParser.hpp"

#include "../productions/SourceFile.hpp"
#include "../productions/Instructions.hpp"
#include "../info/Iaddress.hpp"


class SourceFileParser: public ProductionParser
{
	public:

		Production* safeParse(std::ifstream& inputFile);

	private:

		/** Parser instrukcji */
		InstructionsParser instructionsParser;

		/**
		 * Produkcja <begin_text>. Początek pliku - opis.
		 */
		void ignoreBeginText(std::ifstream& inputFile);

		/**
		 * Produkcja <instructions>.
		 */
		Instructions* parseInstructions(std::ifstream& inputFile);

		/**
		 * Produkcja <end_text>.
		 */
		void ignoreEndText(std::ifstream& inputFile);
};

