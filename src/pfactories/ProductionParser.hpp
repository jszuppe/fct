/*
 * ProductionFactory.hpp
 *
 *  Created on: 25 kwi 2014
 *      Author: jakub
 */

#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <exception>
#include <stdexcept>
#include <limits>

#include <boost/log/trivial.hpp>

#include "../productions/Production.hpp"
#include "../futils.hpp"

class ProductionParser
{
	public:

		ProductionParser()
		{
		}

		virtual ~ProductionParser()
		{
		}

		virtual Production* safeParse(std::ifstream& inputFile) = 0;
};

