/*
 * InstructionParser.hpp
 *
 *  Created on: 30 kwi 2014
 *      Author: jakub
 */

#pragma once

#include "ProductionParser.hpp"
#include "AddressParser.hpp"
#include "OpcodeAndOperandsParser.hpp"

#include "../productions/Instruction.hpp"
#include "../productions/Address.hpp"
#include "../productions/CallAddress.hpp"

#include "../common/utils.hpp"

class InstructionParser: public ProductionParser
{
	private:

		AddressParser addressParser;
		OpcodeAndOperandsParser opcodeOperandsParser;

	public:

		InstructionParser();
		~InstructionParser();
		Production* safeParse(std::ifstream& inputFile);

	private:

		std::string parseInstructionBytes(std::ifstream& inputFile);
		std::string parseLongInstructionBytes(std::ifstream& inputFile);
		static bool isByte(std::string);
};

