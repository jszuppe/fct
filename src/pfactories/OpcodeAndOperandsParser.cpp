/*
 * OpcodeOperandsParser.cpp
 *
 *  Created on: 6 maj 2014
 *      Author: jakub
 */

#include "OpcodeAndOperandsParser.hpp"

OpcodeAndOperandsParser::OpcodeAndOperandsParser()
{

}

OpcodeAndOperandsParser::~OpcodeAndOperandsParser()
{

}

Production * OpcodeAndOperandsParser::safeParse(std::ifstream& inputFile)
{
	inpos svdpos = futils::currentPosition(inputFile);
	std::shared_ptr<OpcodeAndOperands> opop;
	try
	{
		std::string opcode;
		futils::getWord(inputFile, opcode);
		BOOST_LOG_TRIVIAL(debug)<< "OpcodeOperandsParser:\t" << "opcode:\t" << opcode;

		opcodeType opt = OpcodeAndOperands::opcodeTypeFromString(opcode);
		if (opt == opcodeType::Call)
		{
			BOOST_LOG_TRIVIAL(debug)<< "OpcodeOperandsParser:\t" << "opcode type:\tCall";
			// Był call z adresem
			std::shared_ptr<CallAddress> caddress(static_cast<CallAddress*>(callAddressParser.safeParse(inputFile)));
			if(caddress.get() != nullptr)
			{
				BOOST_LOG_TRIVIAL(debug)<< "OpcodeOperandsParser:\t" << "CallAddress:\t" << caddress->strAddress;
				return new CallOpcode(*caddress);
			}

			// Call z rejestrami itd, nie obchodzi on nas.
			futils::igonoreToNewLine(inputFile);
			return new OtherOpcode();
		}
		else if (opt == opcodeType::Ret)
		{
			futils::igonoreToNewLine(inputFile);
			return new RetOpcode();
		}
		else
		{
			futils::igonoreToNewLine(inputFile);
			return new OtherOpcode();
		}
	}
	catch (...)
	{
		futils::restorePosition(inputFile, svdpos);
		std::throw_with_nested(std::logic_error("Error occurred while OpcodeOperandsParser::safeParse"));
	}
	futils::restorePosition(inputFile, svdpos);
	return nullptr;
}

