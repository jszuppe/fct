/*
 * InstructionParser.cpp
 *
 *  Created on: 30 kwi 2014
 *      Author: jakub
 */

#include "InstructionParser.hpp"

InstructionParser::InstructionParser()
{

}

InstructionParser::~InstructionParser()
{

}

Production* InstructionParser::safeParse(std::ifstream& inputFile)
{
	/*
	 <instruction> = <instruction_line> <long_instruction_bytes>{0,2}
	 <instruction_line> = <address> „:” <instruction_bytes> <opcode_and_operands>
	 „\n”
	 <long_instruction_bytes> = <byte>{1,6} „\n”
	 <addres> = [0-9A-F]{8} | [0-9A-F]{16}
	 <instruction_bytes> = <byte>{1,6}
	 <byte> = [0-9A-F]{2}
	 */
	inpos svdpos = futils::currentPosition(inputFile);

	std::shared_ptr<Address> instrAddr;
	std::string instuctionBytes;
	std::shared_ptr<OpcodeAndOperands> opop;

	try
	{
		instrAddr = std::shared_ptr<Address>(static_cast<Address*>(addressParser.safeParse(inputFile)));
		if (instrAddr.get() == nullptr)
		{
			futils::restorePosition(inputFile, svdpos);
			return nullptr;
		}
		BOOST_LOG_TRIVIAL(debug)<< "InstructionParser:\tinstrAddr:" << instrAddr->strAddress;

		char c;
		futils::getChar(inputFile, &c);
		if (c != ':')
		{
			futils::restorePosition(inputFile, svdpos);
			return nullptr;
		}

		instuctionBytes = parseInstructionBytes(inputFile);
		if (instuctionBytes.empty())
		{
			futils::restorePosition(inputFile, svdpos);
			return nullptr;
		}
		BOOST_LOG_TRIVIAL(debug)<< "InstructionParser:\tinstuctionBytes:" << instuctionBytes;

		opop = std::shared_ptr<OpcodeAndOperands>(
				static_cast<OpcodeAndOperands*>(opcodeOperandsParser.safeParse(inputFile)));
		if (opop == nullptr)
		{
			return nullptr;
		}

		if (!inputFile.eof())
		{
			futils::getChar(inputFile, &c);
			if (c != '\n')
			{

				futils::restorePosition(inputFile, svdpos);
				return nullptr;
			}

			instuctionBytes += parseLongInstructionBytes(inputFile);
			BOOST_LOG_TRIVIAL(debug)<< "InstructionParser:\tinstuctionBytes:" << instuctionBytes;
		}
		return new Instruction(opop, *instrAddr, instuctionBytes);

	}
	catch (...)
	{
		futils::restorePosition(inputFile, svdpos);
		std::throw_with_nested(std::logic_error("Error occurred while InstructionsParser::safeParse"));
	}
	futils::restorePosition(inputFile, svdpos);
	return nullptr;
}

std::string InstructionParser::parseInstructionBytes(std::ifstream& inputFile)
{
	inpos svdpos = futils::currentPosition(inputFile);
	std::string ibytes;
	std::string byte;
	futils::getWord(inputFile, byte);
	const int maxBytes = 6;
	while (isByte(byte) && ibytes.size() <= 2 * maxBytes)
	{
		svdpos = futils::currentPosition(inputFile);
		ibytes += byte;
		futils::getWord(inputFile, byte);
	}
	futils::restorePosition(inputFile, svdpos);
	return ibytes;
}

std::string InstructionParser::parseLongInstructionBytes(std::ifstream& inputFile)
{
	inpos svdpos = futils::currentPosition(inputFile);
	// Parsuje linie bajtów
	std::string bytes = parseInstructionBytes(inputFile);
	if (!bytes.empty())
	{
		// Sprawdzenie, czy za bajtami niczego nie ma
		// tylko przejście do nowej linii
		std::string empty_line;
		std::getline(inputFile, empty_line, '\n');
		empty_line = trim(empty_line, " \t\r");
		if (!empty_line.empty())
		{
			futils::restorePosition(inputFile, svdpos);
		}
		svdpos = futils::currentPosition(inputFile);
		// Jeżeli była pełna linia bajtów, tj 6 bajtów
		// to może być druga linia.
		if (bytes.size() == 12)
		{
			bytes += parseInstructionBytes(inputFile);
			if (!bytes.empty())
			{
				empty_line.clear();
				std::getline(inputFile, empty_line, '\n');
				empty_line = trim(empty_line, " \t\r");
				if (!empty_line.empty())
				{
					futils::restorePosition(inputFile, svdpos);
				}
			}
		}
	}
	return bytes;
}

bool InstructionParser::isByte(std::string byte)
{
	return (byte.size() == 2) && byte.find_first_not_of("0123456789abcdefABCDEF") == std::string::npos;
}
