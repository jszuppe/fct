/*
 * OpcodeOperandsParser.hpp
 *
 *  Created on: 6 maj 2014
 *      Author: jakub
 */

#pragma once

#include "ProductionParser.hpp"
#include "CallAddressParser.hpp"
#include "../productions/OpcodeAndOperands.hpp"
#include "../productions/CallAddress.hpp"

class OpcodeAndOperandsParser: public ProductionParser
{
	private:

		CallAddressParser callAddressParser;

	public:
		OpcodeAndOperandsParser();
		~OpcodeAndOperandsParser();
		Production * safeParse(std::ifstream& inputFile);
};

