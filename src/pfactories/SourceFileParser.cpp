/*
 * SourceFileFactory.cpp
 *
 *  Created on: 25 kwi 2014
 *      Author: jakub
 */

#include "SourceFileParser.hpp"

Production* SourceFileParser::safeParse(std::ifstream& inputFile)
{
	inpos svdpos = futils::currentPosition(inputFile);
	try
	{
		ignoreBeginText(inputFile);
		std::shared_ptr<Instructions> instructions(parseInstructions(inputFile));
		ignoreEndText(inputFile);
		// Plik nie został cały przerobiony, co oznacza, że coś poszło nie tak
		// Mimo to, sparsowane do tej pory instrukcje mogą być zapisane.
		if (!inputFile.eof())
		{
			std::string line;
			futils::safeGetLine(inputFile, line);
			BOOST_LOG_TRIVIAL(warning)
			<< "Problem occurred while parsing.";
			BOOST_LOG_TRIVIAL(warning)
			<< "Parsing stopped at " << futils::currentPosition(inputFile)
			<< " character.";
			BOOST_LOG_TRIVIAL(warning)
			<< "Line: \" " << line << "\"";
		}
		if (instructions != nullptr)
		{
			return new SourceFile(*instructions);
		}
		return nullptr;
	}
	catch (...)
	{
		futils::restorePosition(inputFile, svdpos);
		std::throw_with_nested(std::logic_error("Error occurred while SourceFileParser::safeParse"));
	}
	return nullptr;
}

void SourceFileParser::ignoreBeginText(std::ifstream& inputFile)
{
	inpos svdpos = futils::currentPosition(inputFile);

	std::string line, addr;
	futils::getWord(inputFile, line);
	addr = line.substr(0, 8);

	BOOST_LOG_TRIVIAL(debug)<< "parseBeginText:\t " << " addr:\t|" << addr << "|";

	while (!Iaddress::isAddress(addr) && !inputFile.eof())
	{
		futils::restorePosition(inputFile, svdpos);
		// Ignoruj wszystko do nowej linii
		// ignore "zjada" też \n
		// \r występuje przed \n w Windows, więc \r też zjada
		inputFile.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		// Przeszliśmy przez całą linię więc możemy zapisać nową pozycję
		svdpos = futils::currentPosition(inputFile);

		BOOST_LOG_TRIVIAL(debug)<< "parseBeginText:\t" << line;

		futils::getWord(inputFile, line);
		addr = line.substr(0, 8);
		BOOST_LOG_TRIVIAL(debug)<< "parseBeginText:\t " << " addr:\t|" << addr << "|";
	}
	futils::restorePosition(inputFile, svdpos);
}

Instructions * SourceFileParser::parseInstructions(std::ifstream& inputFile)
{
	inpos svdpos = futils::currentPosition(inputFile);
	Instructions * ins = static_cast<Instructions*>(instructionsParser.safeParse(inputFile));
	if (ins == nullptr)
	{
		futils::restorePosition(inputFile, svdpos);
	}
	return ins;
}

void SourceFileParser::ignoreEndText(std::ifstream& inputFile)
{
	if (inputFile.eof())
	{
		return;
	}

	inpos svdpos = futils::currentPosition(inputFile);
	std::string line, addr;
	futils::safeGetLine(inputFile, line);
	addr = line.substr(0, 8);

	// Te sprawdzenie nie jest potrzebne
	// Istnieje tylko dla wyraźniejszego mapowania produkcji <end_text>
	// na metodę ignoreEndText().
	while (!Iaddress::isAddress(addr) && !inputFile.eof())
	{
		futils::restorePosition(inputFile, svdpos);
		// Ignoruj wszystko do EOF
		inputFile.ignore(std::numeric_limits<std::streamsize>::max(), EOF);
		// Linia od której zaczęło się ignorowanie. Tylko dla celów debugowy.
		BOOST_LOG_TRIVIAL(debug)
		<< "parseEndText:\t" << line;
		return;
	}
	futils::restorePosition(inputFile, svdpos);
}

