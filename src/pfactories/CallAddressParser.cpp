/*
 * CallAddressParser.cpp
 *
 *  Created on: 7 maj 2014
 *      Author: jakub
 */

#include "CallAddressParser.hpp"

std::string CallAddressParser::wordSizes[] =
		{
				"byte",
				"word",
				"dword",
				"qword"
		};
std::string CallAddressParser::segments[] =
		{
				"ss",
				"cs",
				"ds",
				"es",
				"fs",
				"gs"
		};

CallAddressParser::CallAddressParser()
{

}

CallAddressParser::~CallAddressParser()
{

}

Production * CallAddressParser::safeParse(std::ifstream& inputFile)
{
	inpos svdpos = futils::currentPosition(inputFile);
	inpos local_svdpos = futils::currentPosition(inputFile);
	try
	{
		// Simple call
		// call	0000000140001730
		std::shared_ptr<Address> simple_address(static_cast<Address*>(addressParser.safeParse(inputFile)));
		if(simple_address.get()!= nullptr)
		{
			return new CallAddress(simple_address->strAddress);
		}

		// Long call
		// call	qword ptr gs:[40002090h]
		std::string wordSize;
		futils::getWord(inputFile, wordSize);
		BOOST_LOG_TRIVIAL(debug)<< "CallAddressParser:\t" << "wordSize:\t|" << wordSize << "|";
		if (!isWordSize(wordSize))
		{
			wordSize = "";
			futils::restorePosition(inputFile, local_svdpos);
		}
		local_svdpos = futils::currentPosition(inputFile);

		std::string ptr;
		futils::getWord(inputFile, ptr);
		BOOST_LOG_TRIVIAL(debug)<< "CallAddressParser:\t" << "ptr:\t|" << ptr << "|";
		if (!isPtr(ptr))
		{
			futils::restorePosition(inputFile, svdpos);
			return nullptr;
		}
		local_svdpos = futils::currentPosition(inputFile);

		std::string seg;
		futils::getChars(inputFile, 3, seg);
		BOOST_LOG_TRIVIAL(debug)<< "CallAddressParser:\t" << "segment:\t|" << seg << "|";
		if (!isSegment(seg))
		{
			seg = "";
			futils::restorePosition(inputFile, local_svdpos);
		}
		local_svdpos = futils::currentPosition(inputFile);

		char c;
		futils::getChar(inputFile, &c);
		BOOST_LOG_TRIVIAL(debug)<< "CallAddressParser:\t" << "address:\t|" << c << "|";
		if (c != '[')
		{
			futils::restorePosition(inputFile, svdpos);
			return nullptr;
		}

		std::string address = parseAddress(inputFile);
		BOOST_LOG_TRIVIAL(debug)<< "CallAddressParser:\t" << "address:\t|" << address << "|";

		futils::getChar(inputFile, &c);
		BOOST_LOG_TRIVIAL(debug)<< "CallAddressParser:\t" << "address:\t|" << c << "|";
		if (c != ']')
		{
			futils::restorePosition(inputFile, svdpos);
			return nullptr;
		}
		BOOST_LOG_TRIVIAL(debug)<< "CallAddressParser:\t"
		<< "CallAddress:\t|" << wordSize << "," << address << "," << seg << "|";
		return new CallAddress(address, wordSize, seg);
	} catch (...)
	{
		futils::restorePosition(inputFile, svdpos);
		std::throw_with_nested(std::logic_error("Error occurred while CallAddressParser::safeParse"));
	}
	futils::restorePosition(inputFile, svdpos);
	return nullptr;
}
