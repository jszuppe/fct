/*
 * AddressParser.cpp
 *
 *  Created on: 30 kwi 2014
 *      Author: jakub
 */

#include "AddressParser.hpp"

AddressParser::AddressParser()
{

}

AddressParser::~AddressParser()
{

}

Production* AddressParser::safeParse(std::ifstream& inputFile)
{
	inpos svdpos = futils::currentPosition(inputFile);

	std::string addr;
	char c;
	for (int i = 0; i < 8; i++)
	{
		futils::getChar(inputFile, &c);
		addr += c;
	}
	if (isAddress(addr))
	{
		svdpos = futils::currentPosition(inputFile);
		for (int i = 0; i < 8; i++)
		{
			futils::getChar(inputFile, &c);
			addr += c;
		}
		if (isAddress(addr))
		{
			return new Address(addr);
		}
		futils::restorePosition(inputFile, svdpos);
		return new Address(addr.substr(0, 8));
	}
	futils::restorePosition(inputFile, svdpos);
	return nullptr;
}

bool AddressParser::isAddress(const std::string& str)
{
	try
	{
		// Czy ma 8 lub 16 znaków i jest liczbą heksadecymalną
		return (str.size() == 8 || str.size() == 16)
				&& str.find_first_not_of("0123456789abcdefABCDEF") == std::string::npos;
	}
	catch (...)
	{
		// wpp.
		return false;
	}
}
