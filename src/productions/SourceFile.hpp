/*
 * SourceFile.hpp
 *
 *  Created on: 25 kwi 2014
 *      Author: jakub
 */

#pragma once

#include "Production.hpp"
#include "Instructions.hpp"

class SourceFile: public Production
{
	private:

		Instructions instructions;

	public:

		SourceFile(Instructions& _instructions) :
				instructions(_instructions)
		{

		}
		std::list<FuncCall> getFuncCallsList();
		std::list<SysCall> getSysCallsList();
		std::list<Iaddress> getRetList();
};

