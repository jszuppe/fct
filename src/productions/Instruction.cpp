/*
 * Instruction.cpp
 *
 *  Created on: 28 kwi 2014
 *      Author: jakub
 */

#include "Instruction.hpp"

Instruction::~Instruction()
{

}

bool Instruction::isRet()
{
	return opcodeAndOperands->isRet();
}
bool Instruction::isCall()
{
	return opcodeAndOperands->isCall();
}

std::string Instruction::getAddressStr()
{
	return address.strAddress;
}
