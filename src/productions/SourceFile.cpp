/*
 * SourceFile.cpp
 *
 *  Created on: 25 kwi 2014
 *      Author: jakub
 */

#include "SourceFile.hpp"

std::list<FuncCall> SourceFile::getFuncCallsList()
{
	return instructions.getFuncCallsList();
}
std::list<SysCall> SourceFile::getSysCallsList()
{
	return instructions.getSysCallsList();
}
std::list<Iaddress> SourceFile::getRetList()
{
	return instructions.getRetList();
}
