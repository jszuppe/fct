/*
 * OpcodeOperands.hpp
 *
 *  Created on: 30 kwi 2014
 *      Author: jakub
 */

#pragma once

#include "Production.hpp"
#include "CallAddress.hpp"
#include <cstdint>
#include <string>

enum opcodeType
{
	Call,
	Ret,
	Other
} typedef opcodeType;

class OpcodeAndOperands: public Production
{
	public:
		virtual opcodeType getOpcodeType() = 0;
		static opcodeType opcodeTypeFromString(std::string& typeStr);
		bool isRet();
		bool isCall();
};

class RetOpcode: public OpcodeAndOperands
{
	public:
		int32_t number;

		RetOpcode(int32_t number = -1);
		enum opcodeType getOpcodeType();
};

class CallOpcode: public OpcodeAndOperands
{
	public:
		CallAddress address;

		CallOpcode(CallAddress&);
		enum opcodeType getOpcodeType();
};

class OtherOpcode: public OpcodeAndOperands
{
	public:
		enum opcodeType getOpcodeType();
};
