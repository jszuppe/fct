/*
 * OpcodeOperands.cpp
 *
 *  Created on: 30 kwi 2014
 *      Author: jakub
 */

#include "OpcodeAndOperands.hpp"

opcodeType OpcodeAndOperands::opcodeTypeFromString(std::string& typeStr)
{
	if (typeStr == "call")
	{
		return opcodeType::Call;
	}
	else if (typeStr == "ret")
	{
		return opcodeType::Ret;
	}
	else
	{
		return opcodeType::Other;
	}
}

bool OpcodeAndOperands::isRet()
{
	return opcodeType::Ret == getOpcodeType();
}
bool OpcodeAndOperands::isCall()
{
	return opcodeType::Call == getOpcodeType();
}


CallOpcode::CallOpcode(CallAddress& address)
: address(address)
{

}

enum opcodeType CallOpcode::getOpcodeType()
{
	return opcodeType::Call;
}

RetOpcode::RetOpcode(int32_t number)
: number(number)
{

}

enum opcodeType RetOpcode::getOpcodeType()
{
	return opcodeType::Ret;
}

enum opcodeType OtherOpcode::getOpcodeType()
{
	return opcodeType::Other;
}
