/*
 * Instructions.cpp
 *
 *  Created on: 25 kwi 2014
 *      Author: jakub
 */

#include "Instructions.hpp"

Instructions::~Instructions()
{

}

void Instructions::addInstruction(Instruction& instruction)
{
	if (instruction.isCall())
	{
		Iaddress address(instruction.getAddressStr());
		CallOpcode * opcode = static_cast<CallOpcode*>(instruction.opcodeAndOperands.get());
		if (opcode->address.isSyscallAddress())
		{
			Iaddress callAddress(opcode->address.strAddress);
			SysCall sysCall(address, opcode->address.wordSize, opcode->address.segment, callAddress);
			sysCallsList.push_back(sysCall);
		}
		else
		{
			Iaddress funcAddress(opcode->address.strAddress);
			funcCallsList.push_back(FuncCall(address, funcAddress));
		}
	}
	else if (instruction.isRet())
	{
		retList.push_back(Iaddress(instruction.getAddressStr()));
	}
}

std::list<FuncCall> Instructions::getFuncCallsList()
{
	return funcCallsList;
}

std::list<SysCall> Instructions::getSysCallsList()
{
	return sysCallsList;
}
std::list<Iaddress> Instructions::getRetList()
{
	return retList;
}
