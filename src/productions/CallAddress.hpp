/*
 * CallAddress.hpp
 *
 *  Created on: 30 kwi 2014
 *      Author: jakub
 */

#pragma once

#include "Production.hpp"
#include <string>

class CallAddress: public Production
{
	private:

		bool syscallAddres;

	public:

		const std::string strAddress;
		const std::string wordSize;
		const std::string segment;

		CallAddress(const std::string  strAddress, const std::string wordSize = std::string(""), const std::string segment = std::string(""))
		:
				strAddress(strAddress), wordSize(wordSize), segment(segment)
		{
			if (wordSize.empty() && segment.empty())
			{
				syscallAddres = false;
			}
			else
			{
				syscallAddres = true;
			}
		}
		~CallAddress();
		bool isSyscallAddress() const;




};
