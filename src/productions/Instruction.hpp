/*
 * Instruction.h
 *
 *  Created on: 28 kwi 2014
 *      Author: jakub
 */

#pragma once

#include "Production.hpp"
#include "OpcodeAndOperands.hpp"
#include "Address.hpp"
#include <memory>

class Instruction: public Production
{
	public:

		const std::shared_ptr<OpcodeAndOperands> opcodeAndOperands;
		const Address  address;
		const std::string instructionBytes;

		Instruction(const std::shared_ptr<OpcodeAndOperands> opcodeAndOperands, const Address&  address,
				const std::string&  instructionBytes)
		: opcodeAndOperands(opcodeAndOperands), address(address), instructionBytes(instructionBytes)
		{

		}
		~Instruction();

		bool isRet();
		bool isCall();
		std::string getAddressStr();
};

