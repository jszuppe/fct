/*
 * Instructions.hpp
 *
 *  Created on: 25 kwi 2014
 *      Author: jakub
 */

#pragma once

#include "Production.hpp"

#include "../info/FuncCall.hpp"
#include "../info/SysCall.hpp"
#include "../info/Iaddress.hpp"

#include "Instruction.hpp"

#include <list>

class Instructions: public Production
{
	private:

		/** Lista z danymi o wywołaniach funkcji*/
		std::list<FuncCall> funcCallsList;
		/** Lista z danymi o wywołaniach funkcji systemowej */
		std::list<SysCall> sysCallsList;
		/** Lista adresów instrukcji ret */
		std::list<Iaddress> retList;

	public:

		Instructions()
		{

		}
		Instructions(std::list<FuncCall> _funcCallsList, std::list<SysCall> _sysCallsList,
				std::list<Iaddress> _retList)
		:
				funcCallsList(_funcCallsList), sysCallsList(_sysCallsList), retList(_retList)
		{

		}
		~Instructions();
		void addInstruction(Instruction& instruction);
		std::list<FuncCall> getFuncCallsList();
		std::list<SysCall> getSysCallsList();
		std::list<Iaddress> getRetList();
};

