/*
 * Address.hpp
 *
 *  Created on: 30 kwi 2014
 *      Author: jakub
 */

#pragma once

#include "Production.hpp"
#include <string>

class Address: public Production
{
	public:
		Address(std::string _strAddress)
		:
				strAddress(_strAddress)
		{

		}
		~Address();

		const std::string strAddress;
};



