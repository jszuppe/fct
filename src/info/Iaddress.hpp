/*
 * Iadress.hpp
 *
 *  Created on: 19 kwi 2014
 *      Author: jakub
 */

#pragma once

#include <cstdint>
#include <string>
#include <iostream>

class Iaddress
{
	public:

		uint64_t addr;
		std::string addrStr;

		Iaddress(const uint64_t addr, const std::string& addrStr) :
				addr(addr), addrStr(addrStr)
		{

		}
		Iaddress(const std::string& addrStr, const int base = 16);

		uint64_t getValue() const;
		std::string getString() const;

		bool operator==(const Iaddress &iaddr2) const;
		bool operator!=(const Iaddress &iaddr2) const;
		bool operator>(const Iaddress &iaddr2) const;
		bool operator<=(const Iaddress &iaddr2) const;
		bool operator<(const Iaddress &iaddr2) const;
		bool operator>=(const Iaddress &iaddr2) const;

		static bool isAddress(const std::string& str);
};

