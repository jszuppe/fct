/*
 * FuncCall.hpp
 *
 *  Created on: 25 kwi 2014
 *      Author: jakub
 */

#pragma once

#include "Iaddress.hpp"

class FuncCall
{
	public:
		FuncCall(Iaddress address, Iaddress funcAddress)
		: address(address), funcAddress(funcAddress)
		{

		};
		~FuncCall();

		Iaddress address;
		Iaddress funcAddress;
};

