/*
 * SysCall.hpp
 *
 *  Created on: 25 kwi 2014
 *      Author: jakub
 */

#pragma once

#include "Iaddress.hpp"
#include <string>

class SysCall
{
	public:

		SysCall(Iaddress address, std::string wordSize, std::string segment, Iaddress funcAddress) :
					address(address), wordSize(wordSize), segment(segment), funcAddress(funcAddress)
		{

		};

		~SysCall();

		Iaddress address;
		std::string wordSize;
		std::string segment;
		Iaddress funcAddress;
};

