/*
 * Iadress.cpp
 *
 *  Created on: 19 kwi 2014
 *      Author: jakub
 */

#include "Iaddress.hpp"

Iaddress::Iaddress(const std::string& _addrStr, const int base)
:
		addr(std::stoull(_addrStr, 0, base)), addrStr(_addrStr)
{

}

uint64_t Iaddress::getValue() const
{
	return addr;
}

std::string Iaddress::getString() const
{
	return addrStr;
}

bool Iaddress::operator==(const Iaddress &iaddr2) const
{
	return addr == iaddr2.getValue();
}

bool Iaddress::operator!=(const Iaddress &iaddr2) const
{
	return addr != iaddr2.getValue();
}

bool Iaddress::operator>(const Iaddress &iaddr2) const
{
	return addr > iaddr2.getValue();
}

bool Iaddress::operator<=(const Iaddress &iaddr2) const
{
	return addr <= iaddr2.getValue();
}

bool Iaddress::operator<(const Iaddress &iaddr2) const
{
	return addr < iaddr2.getValue();
}

bool Iaddress::operator>=(const Iaddress &iaddr2) const
{
	return addr >= iaddr2.getValue();
}

bool Iaddress::isAddress(const std::string& str)
{
	try
	{
		// Czy ma 8 lub 16 znaków i jest liczbą heksadecymalną
		return (str.size() == 8 || str.size() == 16)
				&& str.find_first_not_of("0123456789abcdefABCDEF") == std::string::npos;
	}
	catch (...)
	{
		// wpp.
		return false;
	}
}

