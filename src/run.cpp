/*
 * run.cpp
 *
 *  Created on: 10 kwi 2014
 *      Author: Jakub Szuppe
 */

#include "run.hpp"

void initlogs(const boost::log::trivial::severity_level severity_level)
{
	boost::log::core::get()->set_filter(boost::log::trivial::severity >= severity_level);
}

void silent_logs()
{
	boost::log::core::get()->set_filter(boost::log::trivial::severity > boost::log::trivial::fatal);
}

void validate(boost::any& v, const std::vector<std::string>& values, AsmFileAnalyze::outputFiles* /* target_type */,
		int)
{
	namespace po = boost::program_options;

	// Make sure no previous assignment to 'v' was made.
	po::validators::check_first_occurrence(v);

	if (values.size() != 3)
	{
		throw po::validation_error(po::validation_error::invalid_option_value);
	}

	v = boost::any(AsmFileAnalyze::outputFiles(values[0], values[1], values[2]));
}

int run(int argc, char **argv)
{
	namespace po = boost::program_options;

	try
	{
		// WARNINGS AND UP
		initlogs(boost::log::trivial::info);

		po::options_description general("General options");
		general.add_options()
		("help,h", "Produce help message.")
		("input", po::value<std::string>()->required(), "Input file. [Required]")
		("output,o", po::value<AsmFileAnalyze::outputFiles>()->multitoken(), "Output files.")
				;

		po::options_description special("Special options");
		special.add_options()
		("debug,d", po::bool_switch()->default_value(false), "Print debug logs.")
		("silent,s", po::bool_switch()->default_value(false), "Silent all logs.")
				;

		po::options_description all("Allowed options");
		all.add(general).add(special);

		po::positional_options_description p;
		p.add("input", -1);

		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).
				options(all).positional(p).run(), vm);

		if (vm.count("help"))
		{
			std::cout << all << "\n";
			return 0;
		}

		po::notify(vm);

		if (vm["debug"].as<bool>())
		{
			initlogs(boost::log::trivial::debug);
		}

		if (vm["silent"].as<bool>())
		{
			silent_logs();
		}

		if (vm.count("input"))
		{
			try
			{
				std::string inputFile = vm["input"].as<std::string>();
				BOOST_LOG_TRIVIAL(debug)<< "input file: " << inputFile;

				std::chrono::time_point<std::chrono::system_clock> start, end;
				start = std::chrono::system_clock::now();

				BOOST_LOG_TRIVIAL(info)<< "Parsing started.";

				AsmFileAnalyze afa(inputFile);
				afa.run();

				end = std::chrono::system_clock::now();
				std::chrono::duration<double> elapsed_seconds = end-start;

				BOOST_LOG_TRIVIAL(info)<< "Parsing done.";
				BOOST_LOG_TRIVIAL(info)<< "Elapsed time: " << elapsed_seconds.count() << "s.";

				if (vm.count("output"))
				{
					AsmFileAnalyze::outputFiles outputFiles = vm["output"].as<AsmFileAnalyze::outputFiles>();
					BOOST_LOG_TRIVIAL(debug)<< "FILE calls: "<< outputFiles.callsFileName;
					BOOST_LOG_TRIVIAL(debug)<< "FILE syscalls: "<< outputFiles.syscallsFileName;
					BOOST_LOG_TRIVIAL(debug)<< "FILE functions: "<< outputFiles.funcsFileName;
					afa.saveResults(outputFiles);
				}
				else
				{
					afa.saveResults({"calls", "syscalls","funcs"});
				}
				BOOST_LOG_TRIVIAL(info)<< "Results saved.";
				return 0;
			}
			catch (std::exception& e)
			{
				print_exception(e);
				return 1;
			}
		}
		return 0;

	}
	catch (po::error& e)
	{
		std::cout << "Program options error occurred: " << e.what() << ".\n";
		std::cout << "Use --help option to see help.\n";
		return 0;
	}
}

