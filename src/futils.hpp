/*
 * futils.hpp
 *
 *  Created on: 18 kwi 2014
 *      Author: jakub
 */

#pragma once

#include <iostream>
#include <string>
#include "common/utils.hpp"

typedef std::ios::pos_type inpos;

namespace futils
{
	inline std::ios::pos_type currentPosition(std::istream& input)
	{
		return input.tellg();
	}
	std::ios::pos_type restorePosition(std::istream& input, std::ios::pos_type position);
	std::istream& getChar(std::istream& input, char* c);
	std::istream& safeGetLine(std::istream& input, std::string& line);
	std::istream& getWord(std::istream& input, std::string& line);
	/**
	 * Wczytuje najwyżej n znaków do line.
	 * Może wczytać mniej, gdy napotka '\n' lub '\r'.
	 * @param input
	 * @param n
	 * @param line
	 * @return input
	 */
	std::istream& getChars(std::istream& input, unsigned int n, std::string& line);
	std::istream& igonoreToNewLine(std::istream& input);
}
