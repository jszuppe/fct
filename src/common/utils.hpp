/*
 * utils.hpp
 *
 *  Created on: 10 kwi 2014
 *      Author: jakub
 */

#pragma once

#include <stdexcept>
#include <exception>
#include <iostream>

/**
 * Prints the explanatory string of an exception. If the exception is nested,
 * recurses to print the explanatory of the exception it holds.
 *
 * @param e
 * @param level
 */
void print_exception(const std::exception& e, int level = 0);

/**
 * Trim string
 * @param str
 * @param whitespace
 * @return trimmed str
 */
std::string trim(const std::string& str, const std::string& whitespace = " \t");
