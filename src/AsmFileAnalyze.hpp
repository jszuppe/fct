/*
 * AsmFileAnalyze.hpp
 *
 *  Created on: 10 kwi 2014
 *      Author: jakub
 */

#pragma once

#include <iostream>
#include <string>
#include <exception>
#include <stdexcept>
#include <limits>
#include <list>
#include <algorithm>
#include <iterator>

#include <boost/log/trivial.hpp>

#include "info/Iaddress.hpp"
#include "futils.hpp"
#include "productions/SourceFile.hpp"
#include "pfactories/SourceFileParser.hpp"

class AsmFileAnalyze
{
	private:

		/** Plik wejściowy. */
		std::ifstream inputFile;

		/** Plik */
		std::unique_ptr<SourceFile> source;

		/** Znalezione funkcje jako pary (adres początkowy, adres końcowy) */
		std::list<std::pair<Iaddress, Iaddress>> funcs;

	public:

		typedef std::ios::pos_type inpos;

		/**
		 * Konstruktor. Sprawdza istnienie pliku.
		 * @param inputFilePath - ścieżka do pliku asm, który ma zostać zbadany.
		 */
		AsmFileAnalyze(std::string inputFilePath);

		/**
		 * Destruktor.
		 */
		~AsmFileAnalyze();

		struct outputFiles
		{
				std::string callsFileName;
				std::string syscallsFileName;
				std::string funcsFileName;

				outputFiles(std::string const& cfn, std::string const& scfn,
						std::string const& ffn) :
						callsFileName(cfn), syscallsFileName(scfn), funcsFileName(ffn)
				{

				}
		};

		void run();
		void saveResults(AsmFileAnalyze::outputFiles fileNames);

	private:

		void buildResults();
};
