/*
 * run.hpp
 *
 *  Created on: 10 kwi 2014
 *      Author: jakub
 */

#pragma once

#include <boost/program_options.hpp>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

#include <vector>
#include <sstream>
#include <chrono>

#include "AsmFileAnalyze.hpp"
#include "common/utils.hpp"

/**
 * Overloading the validate function from boost::program_options
 */
void validate(boost::any& v, std::vector<std::string> const& values, AsmFileAnalyze::outputFiles* /* target_type */,
		int);

/**
 * Overloading the validate function from boost::program_options
 */
void validate(boost::any& v, const std::vector<std::string>& values, boost::log::trivial::severity_level* /* target_type */,
		int);

/**
 * All logs are off.
 */
void silent_logs();

/**
 * Init logging settings.
 * @param severity_level
 */
void initlogs(const boost::log::trivial::severity_level severity_level);

/**
 * Run with given argument array.
 * @param argc - number of arguments in argv array.
 * @param argv - arguments array.
 * @return
 */
int run(int argc, char **argv);
