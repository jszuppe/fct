/*
 * futils.cpp
 *
 *  Created on: 18 kwi 2014
 *      Author: jakub
 */

#include "futils.hpp"

std::istream& futils::safeGetLine(std::istream& input, std::string& line)
{
	line.clear();

	std::istream::sentry se(input, true);
	std::streambuf* sb = input.rdbuf();

	for (;;)
	{
		int c = sb->sbumpc();
		switch (c)
		{
			case '\n':
				return input;
			case '\r':
				if (sb->sgetc() == '\n')
					sb->sbumpc();
				line = trim(line);
				return input;
			case EOF:
				if (line.empty())
					input.setstate(std::ios::eofbit);
				line = trim(line);
				return input;
			default:
				line += (char) c;
		}
	}
	return input;
}

std::istream& futils::getChar(std::istream& input, char* c)
{
	input.read(c, 1);
	while (*c == ' ' || *c == '\t' || *c == '\r')
	{
		input.read(c, 1);
	}

	return input;
}

std::ios::pos_type futils::restorePosition(std::istream& input, std::ios::pos_type position)
{
	input.seekg(position);
	return input.tellg();
}

std::istream& futils::getWord(std::istream& input, std::string& line)
{
	line.clear();
	// Wszystkie znaki ' ' '\t'
	char c = ' ';
	inpos svdpos = currentPosition(input);
	while (c == ' ' || c == '\t' || c == '\r')
	{
		svdpos = currentPosition(input);
		input.read(&c, 1);
	}
	restorePosition(input, svdpos);

	input.read(&c, 1);
	while (c != ' ' && c != '\t' && c != '\r' && c != '\n')
	{
		line += c;
		svdpos = currentPosition(input);
		input.read(&c, 1);
	}
	restorePosition(input, svdpos);
	return input;
}

std::istream& futils::getChars(std::istream& input, unsigned int n, std::string& line)
{
	line.clear();
	char c = ' ';
	inpos svdpos = currentPosition(input);
	unsigned i = 0;
	while (i < n)
	{
		getChar(input, &c);
		if (c != '\n' && c != '\r')
		{
			svdpos = currentPosition(input);
			line += c;
			i++;
		}
		else
		{
			restorePosition(input, svdpos);
			break;
		}
	}
	return input;
}

std::istream& futils::igonoreToNewLine(std::istream& input)
{
	inpos svdpos = futils::currentPosition(input);
	char c = ' ';
	while (c != '\n')
	{
		svdpos = futils::currentPosition(input);
		getChar(input, &c);
	}
	futils::restorePosition(input, svdpos);
	return input;
}
